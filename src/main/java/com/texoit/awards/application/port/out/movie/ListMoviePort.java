package com.texoit.awards.application.port.out.movie;

import com.texoit.awards.domain.Movie;

import java.util.List;

public interface ListMoviePort {
    List<Movie> listMovies();
}
