package com.texoit.awards.application.port.in.movie;

import com.texoit.awards.domain.Movie;

import java.util.List;

public interface ListMovieUseCase {
    List<Movie> listMovies();
}
