package com.texoit.awards.application.port.in.movie;

import com.texoit.awards.domain.Movie;
import com.texoit.awards.utility.SelfValidating;
import lombok.EqualsAndHashCode;
import lombok.Value;

import javax.validation.constraints.NotNull;
import java.util.Optional;

public interface CreateMovieUseCase {
    Optional<Movie> create(CreateMovieCommand createMovieCommand);

    @Value
    @EqualsAndHashCode(callSuper = false)
    class CreateMovieCommand extends SelfValidating<CreateMovieCommand> {
        @NotNull
        private final Movie movie;

        public CreateMovieCommand(Movie movie) {
            this.movie = movie;
            validateSelf();
        }
    }
}
