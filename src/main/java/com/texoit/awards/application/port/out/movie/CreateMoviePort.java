package com.texoit.awards.application.port.out.movie;

import com.texoit.awards.domain.Movie;

import java.util.Optional;

public interface CreateMoviePort {
    Optional<Movie> create(Movie movie);
}
