package com.texoit.awards.application.service.movie;

import com.texoit.awards.application.port.in.movie.CreateMovieUseCase;
import com.texoit.awards.application.port.out.movie.CreateMoviePort;
import com.texoit.awards.domain.Movie;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
@RequiredArgsConstructor
public class CreateMovieService implements CreateMovieUseCase {
    private final CreateMoviePort createMoviePort;

    @Override
    public Optional<Movie> create(CreateMovieCommand createMovieCommand) {
        return createMoviePort.create(createMovieCommand.getMovie());
    }
}
