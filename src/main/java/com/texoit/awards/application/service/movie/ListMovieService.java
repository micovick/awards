package com.texoit.awards.application.service.movie;

import com.texoit.awards.application.port.in.movie.ListMovieUseCase;
import com.texoit.awards.application.port.out.movie.ListMoviePort;
import com.texoit.awards.domain.Movie;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@RequiredArgsConstructor
public class ListMovieService implements ListMovieUseCase {
    private final ListMoviePort listMoviePort;

    @Override
    public List<Movie> listMovies() {
        return listMoviePort.listMovies();
    }
}
