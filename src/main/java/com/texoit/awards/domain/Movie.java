package com.texoit.awards.domain;

import com.texoit.awards.utility.SelfValidating;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class Movie extends SelfValidating<Movie> {
    private Long id;

    private final String title;

    private final Integer year;

    private final String studios;

    private final String producers;

    private final String winner;

    public Movie(String title, Integer year, String studios, String producers, String winner) {
        this.title = title;
        this.year = year;
        this.studios = studios;
        this.producers = producers;
        this.winner = winner;

        validateSelf();
    }
}
