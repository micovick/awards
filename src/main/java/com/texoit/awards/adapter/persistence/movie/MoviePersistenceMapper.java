package com.texoit.awards.adapter.persistence.movie;

import com.texoit.awards.domain.Movie;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class MoviePersistenceMapper {

    public Movie mapToDomainEntity(MovieJpaEntity movieJpaEntity){
        Movie movie = new Movie(movieJpaEntity.getTitle(), movieJpaEntity.getYear(), movieJpaEntity.getStudios(), movieJpaEntity.getProducers(), movieJpaEntity.getWinner());
        movie.setId(movieJpaEntity.getId());

        return movie;
    }

    public List<Movie> mapToDomainEntity(List<MovieJpaEntity> movieJpaEntities) {
        return movieJpaEntities.stream().map(this::mapToDomainEntity).collect(Collectors.toList());
    }

    public MovieJpaEntity mapToJpaEntity(Movie movie){
        MovieJpaEntity movieJpaEntity = new MovieJpaEntity();
        movieJpaEntity.setTitle(movie.getTitle());
        movieJpaEntity.setYear(movie.getYear());
        movieJpaEntity.setStudios(movie.getStudios());
        movieJpaEntity.setProducers(movie.getProducers());
        movieJpaEntity.setWinner(movie.getWinner());

        return movieJpaEntity;
    }
}
