package com.texoit.awards.adapter.persistence.movie;

import org.springframework.data.jpa.repository.JpaRepository;

public interface MovieRepository extends JpaRepository<MovieJpaEntity, Long> {
}
