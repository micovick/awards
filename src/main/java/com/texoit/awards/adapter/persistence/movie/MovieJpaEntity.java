package com.texoit.awards.adapter.persistence.movie;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@RequiredArgsConstructor
@Entity(name = "MovieJpaEntity")
@Table(name = "aws_movie")
public class MovieJpaEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String title;

    private Integer year;

    private String studios;

    private String producers;

    private String winner;
}
