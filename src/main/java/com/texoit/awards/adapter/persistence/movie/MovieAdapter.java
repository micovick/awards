package com.texoit.awards.adapter.persistence.movie;

import com.texoit.awards.application.port.out.movie.CreateMoviePort;
import com.texoit.awards.application.port.out.movie.ListMoviePort;
import com.texoit.awards.domain.Movie;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Component
@RequiredArgsConstructor
@Transactional
public class MovieAdapter implements CreateMoviePort, ListMoviePort {

    private final MovieRepository movieRepository;
    private final MoviePersistenceMapper moviePersistenceMapper;

    @Override
    public Optional<Movie> create(Movie movie) {
        return Optional.of(moviePersistenceMapper.mapToDomainEntity(movieRepository.save(moviePersistenceMapper.mapToJpaEntity(movie))));
    }

    @Override
    public List<Movie> listMovies() {
        return null;
    }
}
