package com.texoit.awards;

import lombok.Generated;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AwardsApplication {
    @Generated
    public static void main(String[] args) {
        SpringApplication.run(AwardsApplication.class, args);
    }
}
